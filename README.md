# Statistikkprogram for roulette #

![roulette.jpg](https://bitbucket.org/repo/8zzR4XX/images/4187430685-roulette.jpg)

## Funksjon(er): ##
* Hente og lagre statistikkdata
* Hente og lagre brukerdefinerte tall-grupper
* Vise nummersekvens for tall/nummer
* Vise kolonne for tall/nummer
* Vise dozen (dusin) for tall/nummer (1-12, 13-24, 25-36)
* Vise brukerdefinerte tallgrupper med rød, grønn, svart
* Vise antall spins og prosentfordeling av tall/numre

Teknologier benyttet: Visual Studio 2010, C#, Windows Forms, .Net Framework 2.0, MVP (Model-View-Presenter)