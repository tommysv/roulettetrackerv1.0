﻿using RouletteTracker.Core;
using RouletteTracker.Core.Views;

namespace RouletteTracker.Presenters
{
    public class Presenter<T> where T : IView
    {
        public Presenter(T view, IRouletteTrackerModel model)
        {
            View = view;
            Model = model;
        }

        protected IRouletteTrackerModel Model { get; private set; }

        protected T View { get; private set; }
    }
}
