﻿using System;
using System.Collections.Generic;
using RouletteTracker.Core;
using RouletteTracker.Core.Objects;
using RouletteTracker.Core.Views;

namespace RouletteTracker.Presenters
{
    public class P_Main : Presenter<IV_Main>
    {
        private readonly IV_Main _mainView;
        private readonly IV_SequenceOverview _sequenceOverview;
        private readonly IV_DozenOverview _dozenOverview;
        private readonly IV_ColumnOverview _columnOverview;
        private readonly IV_GroupOverview _groupOverview;
        private readonly IV_GroupSetup _groupSetup;
        private readonly IV_LoadData _loadDataView;

        private IList<int> _numberSequence = new List<int>();

        public P_Main(IV_Main view, IRouletteTrackerModel model, IV_LoadData loadDataView, IV_GroupSetup groupSetup)
            : base(view, model)
        {
            _mainView = view;
            _sequenceOverview = _mainView.SequenceOverview;
            _dozenOverview = _mainView.DozenOverview;
            _columnOverview = _mainView.ColumnOverview;
            _groupOverview = _mainView.GroupOverview;
            _groupSetup = groupSetup;
            _loadDataView = loadDataView;

            _mainView.Load += _mainView_Load;
            _mainView.LoadData += _mainView_LoadData;
            _mainView.Reset += _mainView_Reset;
            _mainView.SaveData += _mainView_SaveData;
            _mainView.Groups += _mainView_Groups;
            _mainView.FormClosed += _mainView_FormClosed;

            groupSetup.Save += groupSetup_Save;
            _sequenceOverview.AddSpin += _sequenceOverview_AddSpin;
            _loadDataView.LoadData += _loadDataView_LoadData;
        }

        private void _loadDataView_LoadData(object sender, EventArgs e)
        {
            _numberSequence = Model.LoadData(_loadDataView.SelectedDataDescriptor);
            _sequenceOverview.NumberSequence = _numberSequence;
            _dozenOverview.NumberSequence = _numberSequence;
            _columnOverview.NumberSequence = _numberSequence;
            _groupOverview.NumberSequence  = _numberSequence;

            NumberPercentages percentages = CalculatePercentages();
            View.NumSpins = Convert.ToString(_numberSequence.Count);
            View.PercentageLo = string.Format("{0:0.#}%", percentages.PercentageLo);
            View.PercentageMe = string.Format("{0:0.#}%", percentages.PercentageMe);
            View.PercentageHi = string.Format("{0:0.#}%", percentages.PercentageHi);
        }

        private void groupSetup_Save(object sender, EventArgs e)
        {
            Model.SaveNumberGroups(_groupSetup.NumberGroups);
            _groupOverview.NumberGroups = _groupSetup.NumberGroups;
        }

        private void _sequenceOverview_AddSpin(object sender, AddSpinEventArgs e)
        {
            if (!_numberSequence.IsReadOnly)
            {
                _numberSequence.Add(e.Number);

                _sequenceOverview.AddNumber(e.Number);
                _dozenOverview.AddNumber(e.Number);
                _columnOverview.AddNumber(e.Number);
                _groupOverview.AddNumber(e.Number);

                NumberPercentages percentages = CalculatePercentages();
                View.NumSpins = Convert.ToString(_numberSequence.Count);
                View.PercentageLo = string.Format("{0:0.#}%", percentages.PercentageLo);
                View.PercentageMe = string.Format("{0:0.#}%", percentages.PercentageMe);
                View.PercentageHi = string.Format("{0:0.#}%", percentages.PercentageHi);
            }
        }

        private void _mainView_Load(object sender, EventArgs e)
        {
            View.NumSpins = "0";
            View.PercentageLo = "0";
            View.PercentageMe = "0";
            View.PercentageHi = "0";
            _groupOverview.NumberGroups = Model.LoadNumberGroups();
        }

        private void _mainView_LoadData(object sender, EventArgs e)
        {
            _loadDataView.DataDescriptors = Model.EnumerateDataDescriptors();
            _loadDataView.Display();
        }

        private void _mainView_SaveData(object sender, EventArgs e)
        {
            Model.SaveData(_numberSequence);
        }

        private void _mainView_Reset(object sender, EventArgs e)
        {
            View.NumSpins = "0";
            View.PercentageLo = "0";
            View.PercentageMe = "0";
            View.PercentageHi = "0";
            _sequenceOverview.ResetView();
            _dozenOverview.ResetView();
            _columnOverview.ResetView();
            _groupOverview.ResetView();

            _numberSequence = new List<int>();
        }

        private void _mainView_Groups(object sender, EventArgs e)
        {
            _groupSetup.NumberGroups = Model.LoadNumberGroups();
            _groupSetup.Display();
        }

        private void _mainView_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            _loadDataView.LoadData -= _loadDataView_LoadData;
            _sequenceOverview.AddSpin -= _sequenceOverview_AddSpin;
            _groupSetup.Save -= groupSetup_Save;
            _mainView.Load -= _mainView_Load;
            _mainView.Reset -= _mainView_Reset;
            _mainView.LoadData -= _mainView_LoadData;
            _mainView.SaveData -= _mainView_SaveData;
            _mainView.Groups -= _mainView_Groups;
            _mainView.FormClosed -= _mainView_FormClosed;
        }

        private NumberPercentages CalculatePercentages()
        {
            NumberPercentages numberPercentages = new NumberPercentages();

            int loCount = 0;
            int meCount = 0;
            int hiCount = 0;

            if (_numberSequence.Count > 0)
            {
                for (int n = 0; n < _numberSequence.Count; n++)
                {
                    if ((_numberSequence[n] >= 1) && (_numberSequence[n] <= 12))
                        loCount++;
                    else if ((_numberSequence[n] >= 13) && (_numberSequence[n] <= 24))
                        meCount++;
                    else if ((_numberSequence[n] >= 25) && (_numberSequence[n] <= 36))
                        hiCount++;
                }
                numberPercentages.PercentageLo = ((double)loCount / _numberSequence.Count) * 100;
                numberPercentages.PercentageMe = ((double)meCount / _numberSequence.Count) * 100;
                numberPercentages.PercentageHi = ((double)hiCount / _numberSequence.Count) * 100;
            }
            return numberPercentages;
        }
    }
}
