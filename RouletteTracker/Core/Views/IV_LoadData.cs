﻿using System;
using System.Collections.Generic;

namespace RouletteTracker.Core.Views
{
    public interface IV_LoadData : IView
    {
        IList<string> DataDescriptors { set; }
        string SelectedDataDescriptor { get; }
        void Display();
        event EventHandler LoadData;
    }
}
