﻿using System;
using System.Windows.Forms;

namespace RouletteTracker.Core.Views
{
    public interface IV_Main : IView
    {
        string NumSpins { set; }
        string PercentageLo { set; }
        string PercentageMe { set; }
        string PercentageHi { set; }

        IV_SequenceOverview SequenceOverview { get; }
        IV_DozenOverview DozenOverview { get; }
        IV_ColumnOverview ColumnOverview { get; }
        IV_GroupOverview GroupOverview { get; }

        event EventHandler Load;
        event EventHandler LoadData;
        event EventHandler Reset;
        event EventHandler SaveData;
        event EventHandler Groups;
        event FormClosedEventHandler FormClosed;
    }
}
