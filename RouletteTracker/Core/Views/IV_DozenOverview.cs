﻿using System.Collections.Generic;

namespace RouletteTracker.Core.Views
{
    public interface IV_DozenOverview : IView
    {
        IList<int> NumberSequence { set; }
        void AddNumber(int number);
        void ResetView();
    }
}
