﻿using System.Collections.Generic;
using RouletteTracker.Core.Objects;

namespace RouletteTracker.Core.Views
{
    public interface IV_GroupOverview : IView
    {
        IList<int> NumberSequence { set; }
        void AddNumber(int number);
        NumberGroups NumberGroups { set; }
        void ResetView();
    }
}
