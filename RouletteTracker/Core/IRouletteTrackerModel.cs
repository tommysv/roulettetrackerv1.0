﻿using System.Collections.Generic;
using RouletteTracker.Core.Objects;

namespace RouletteTracker.Core
{
    public interface IRouletteTrackerModel
    {
        IList<string> EnumerateDataDescriptors();
        IList<int> LoadData(string descriptor);
        void SaveData(IList<int> numbers);
        void SaveNumberGroups(NumberGroups obj);
        NumberGroups LoadNumberGroups();
    }
}
