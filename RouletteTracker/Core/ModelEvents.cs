﻿namespace RouletteTracker.Core
{
    public delegate void AddSpinEventHandler(object sender, AddSpinEventArgs e);
        
    public class AddSpinEventArgs : System.EventArgs
    {
        private int _number;

        public AddSpinEventArgs(int number)
        {
            _number = number;
        }

        public int Number
        {
            get { return _number; }
        }
    }
}
