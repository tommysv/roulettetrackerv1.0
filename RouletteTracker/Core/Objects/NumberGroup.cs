﻿namespace RouletteTracker.Core.Objects
{
    public enum NumberGroup
    {
        None = 0,
        Red = 1,
        Green = 2,
        Black = 3
    }
}
