﻿namespace RouletteTracker.Core.Objects
{
    public class NumberPercentages
    {
        private double _percentageLo;
        private double _percentageMe;
        private double _percentageHi;

        public NumberPercentages()
        {
            _percentageLo = 0;
            _percentageMe = 0;
            _percentageHi = 0;
        }

        public double PercentageLo
        {
            get { return _percentageLo; }
            set { _percentageLo = value; }
        }

        public double PercentageMe
        {
            get { return _percentageMe; }
            set { _percentageMe = value; }
        }

        public double PercentageHi
        {
            get { return _percentageHi; }
            set { _percentageHi = value; }
        }
    }
}
