﻿using System;
using System.Collections.Generic;

namespace RouletteTracker.Core.Objects
{
    [Serializable]
    public class NumberGroups
    {
        private readonly List<int> _redNumbers;
        private readonly List<int> _greenNumbers;
        private readonly List<int> _blackNumbers;

        public NumberGroups()
        {
            _redNumbers = new List<int>();
            _greenNumbers = new List<int>();
            _blackNumbers = new List<int>();
        }

        public List<int> RedNumbers
        {
            get { return _redNumbers; }
        }

        public List<int> GreenNumbers
        {
            get { return _greenNumbers; }
        }

        public List<int> BlackNumbers
        {
            get { return _blackNumbers; }
        }

        public NumberGroup FindNumberGroup(int number)
        {
            for (int n = 0; n < _redNumbers.Count; n++)
            {
                if (_redNumbers[n] == number) return NumberGroup.Red;
            }
            for (int n = 0; n < _greenNumbers.Count; n++)
            {
                if (_greenNumbers[n] == number) return NumberGroup.Green;
            }
            for (int n = 0; n < _blackNumbers.Count; n++)
            {
                if (_blackNumbers[n] == number) return NumberGroup.Black;
            }
            return NumberGroup.None;
        }
    }
}
