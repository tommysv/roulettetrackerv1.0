﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using RouletteTracker.Core;
using RouletteTracker.Core.Objects;

namespace RouletteTracker.Models
{
    public class RouletteTrackerFileModel : IRouletteTrackerModel
    {
        private string _folderPath;

        public RouletteTrackerFileModel()
        {
            _folderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "RouletteTracker");
        }

        // For testing purposes
        public RouletteTrackerFileModel(string folderPath)
        {
              _folderPath = folderPath;
        }

        public IList<string> EnumerateDataDescriptors()
        {
            List<string> dataDescriptors = new List<string>();
            string dataPath = Path.Combine(_folderPath, "DataFiles");
            if (Directory.Exists(dataPath))
            {
                string[] dataFiles = Directory.GetFiles(dataPath, "*.dat", SearchOption.TopDirectoryOnly);
                for (int nFile = 0; nFile < dataFiles.Length; nFile++)
                {
                    dataDescriptors.Add(Path.GetFileNameWithoutExtension(dataFiles[nFile]));
                }
            }
            return new ReadOnlyCollection<string>(dataDescriptors);
        }

        public IList<int> LoadData(string descriptor)
        {
            List<int> data = new List<int>();
            string dataPath = Path.Combine(_folderPath, "DataFiles");
            if (Directory.Exists(dataPath))
            {
                string filePath = Path.Combine(dataPath, descriptor + ".dat");
                if (File.Exists(filePath))
                {
                    using (FileStream fs = File.OpenRead(filePath))
                    {
                        int byteRead = 0;
                        while ((byteRead = fs.ReadByte()) != -1)
                        {
                            data.Add(byteRead);
                        }
                    }
                }
            }
            return new ReadOnlyCollection<int>(data);
        }

        public void SaveData(IList<int> numbers)
        {
            string dataPath = Path.Combine(_folderPath, "DataFiles");
            if ((numbers == null) || (numbers.Count < 1))
                return;
            if (!Directory.Exists(dataPath))
                Directory.CreateDirectory(dataPath);
            string filePath = Path.Combine(dataPath, DateTime.Now.ToString("ddMMyyyy_HH_mm_ss") + ".dat");
            using (FileStream fs = File.OpenWrite(filePath))
            {
                for (int nNumber = 0; nNumber < numbers.Count; nNumber++)
                {
                    fs.WriteByte((byte)numbers[nNumber]);
                }
            }
        }

        public void SaveNumberGroups(NumberGroups obj)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(NumberGroups));
            if (!Directory.Exists(_folderPath))
                Directory.CreateDirectory(_folderPath);
            string filePath = Path.Combine(_folderPath, "NumberGroups.xml");
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                serializer.Serialize(writer, obj);
            }
        }

        public NumberGroups LoadNumberGroups()
        {
            NumberGroups obj = new NumberGroups();
            string filePath = Path.Combine(_folderPath, "NumberGroups.xml");
            if (File.Exists(filePath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(NumberGroups));
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    obj = (NumberGroups)serializer.Deserialize(fs);
                }
            }
            return obj;
        }
    }
}
