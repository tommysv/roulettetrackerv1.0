﻿using System;
using System.Collections.Generic;
using RouletteTracker.Core;
using RouletteTracker.Core.Objects;

namespace RouletteTracker.Models
{
    public class RouletteTrackerDbModel : IRouletteTrackerModel
    {
        private string _connectionString;

        public RouletteTrackerDbModel()
        {
            _connectionString = String.Empty;
        }

        // For testing purposes
        public RouletteTrackerDbModel(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IList<string> EnumerateDataDescriptors()
        {
            throw new System.NotImplementedException();
        }

        public IList<int> LoadData(string descriptor)
        {
            throw new System.NotImplementedException();
        }

        public void SaveData(IList<int> numbers)
        {
            throw new System.NotImplementedException();
        }

        public void SaveNumberGroups(NumberGroups obj)
        {
            throw new System.NotImplementedException();
        }

        public NumberGroups LoadNumberGroups()
        {
            throw new System.NotImplementedException();
        }
    }
}
