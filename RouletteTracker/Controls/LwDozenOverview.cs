﻿using System.Windows.Forms;

namespace RouletteTracker.Controls
{
    public class LwDozenOverview : ListView
    {
        public LwDozenOverview()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

    }
}
