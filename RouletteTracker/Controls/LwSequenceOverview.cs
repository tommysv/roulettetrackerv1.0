﻿using System.Windows.Forms;

namespace RouletteTracker.Controls
{
    public class LwSequenceOverview : ListView
    {
        public LwSequenceOverview()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }
    }
}
