﻿using System.Windows.Forms;

namespace RouletteTracker.Controls
{
    public class LwCustom : ListView
    {
        public LwCustom()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }
    }
}
