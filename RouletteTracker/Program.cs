﻿using System;
using System.Windows.Forms;
using RouletteTracker.Core;
using RouletteTracker.Factories;
using RouletteTracker.Presenters;
using RouletteTracker.Views;

namespace RouletteTracker
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IRouletteTrackerModel model = ModelFactory.CreateModel(ModelType.File);
            DlgGroupSetup dlgGroupSetup = new DlgGroupSetup();
            DlgLoadData dlgLoadData = new DlgLoadData();
            FrmMain mainForm = new FrmMain();
            P_Main mainPresenter = new P_Main(mainForm, model, dlgLoadData, dlgGroupSetup);
            Application.Run(mainForm);
        }
    }
}
