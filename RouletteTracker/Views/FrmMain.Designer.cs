﻿namespace RouletteTracker.Views
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btnReset = new System.Windows.Forms.Button();
            this.lblSequenceDesc = new System.Windows.Forms.Label();
            this.lblNumbersDrawn = new System.Windows.Forms.Label();
            this.lblNumbersDrawn2 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnGroups = new System.Windows.Forms.Button();
            this.uscLabelPercentageHi = new UscLabel();
            this.uscLabelPercentageMe = new UscLabel();
            this.uscLabelPercentageLo = new UscLabel();
            this.uscGroupOverview1 = new UscGroupOverview();
            this.uscColumnOverview = new UscColumnOverview();
            this.uscDozenOverview = new UscDozenOverview();
            this.uscSequenceOverview = new UscSequenceOverview();
            this.uscLabel1 = new UscLabel();
            this.uscLabel2 = new UscLabel();
            this.btnLoadData = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(12, 706);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(83, 23);
            this.btnReset.TabIndex = 5;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            // 
            // lblSequenceDesc
            // 
            this.lblSequenceDesc.AutoSize = true;
            this.lblSequenceDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSequenceDesc.Location = new System.Drawing.Point(12, 344);
            this.lblSequenceDesc.Name = "lblSequenceDesc";
            this.lblSequenceDesc.Size = new System.Drawing.Size(0, 18);
            this.lblSequenceDesc.TabIndex = 12;
            // 
            // lblNumbersDrawn
            // 
            this.lblNumbersDrawn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumbersDrawn.Location = new System.Drawing.Point(175, 9);
            this.lblNumbersDrawn.Name = "lblNumbersDrawn";
            this.lblNumbersDrawn.Size = new System.Drawing.Size(74, 23);
            this.lblNumbersDrawn.TabIndex = 13;
            this.lblNumbersDrawn.Text = "Spins";
            this.lblNumbersDrawn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNumbersDrawn2
            // 
            this.lblNumbersDrawn2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNumbersDrawn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumbersDrawn2.Location = new System.Drawing.Point(241, 10);
            this.lblNumbersDrawn2.Name = "lblNumbersDrawn2";
            this.lblNumbersDrawn2.Size = new System.Drawing.Size(77, 23);
            this.lblNumbersDrawn2.TabIndex = 14;
            this.lblNumbersDrawn2.Text = "0";
            this.lblNumbersDrawn2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(101, 11);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(68, 23);
            this.btnSave.TabIndex = 16;
            this.btnSave.Text = "Lagre";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnGroups
            // 
            this.btnGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGroups.Location = new System.Drawing.Point(588, 14);
            this.btnGroups.Name = "btnGroups";
            this.btnGroups.Size = new System.Drawing.Size(68, 23);
            this.btnGroups.TabIndex = 44;
            this.btnGroups.Text = "Grupper...";
            this.btnGroups.UseVisualStyleBackColor = true;
            // 
            // uscLabelPercentageHi
            // 
            this.uscLabelPercentageHi.LabelText = "25-36";
            this.uscLabelPercentageHi.Location = new System.Drawing.Point(501, 14);
            this.uscLabelPercentageHi.Name = "uscLabelPercentageHi";
            this.uscLabelPercentageHi.Size = new System.Drawing.Size(81, 21);
            this.uscLabelPercentageHi.TabIndex = 51;
            this.uscLabelPercentageHi.Value = "0";
            // 
            // uscLabelPercentageMe
            // 
            this.uscLabelPercentageMe.LabelText = "13-24";
            this.uscLabelPercentageMe.Location = new System.Drawing.Point(411, 14);
            this.uscLabelPercentageMe.Name = "uscLabelPercentageMe";
            this.uscLabelPercentageMe.Size = new System.Drawing.Size(81, 21);
            this.uscLabelPercentageMe.TabIndex = 50;
            this.uscLabelPercentageMe.Value = "0";
            // 
            // uscLabelPercentageLo
            // 
            this.uscLabelPercentageLo.LabelText = "1-12";
            this.uscLabelPercentageLo.Location = new System.Drawing.Point(324, 14);
            this.uscLabelPercentageLo.Name = "uscLabelPercentageLo";
            this.uscLabelPercentageLo.Size = new System.Drawing.Size(81, 21);
            this.uscLabelPercentageLo.TabIndex = 49;
            this.uscLabelPercentageLo.Value = "0";
            // 
            // uscGroupOverview1
            // 
            this.uscGroupOverview1.Location = new System.Drawing.Point(588, 43);
            this.uscGroupOverview1.Name = "uscGroupOverview1";
            this.uscGroupOverview1.Size = new System.Drawing.Size(92, 657);
            this.uscGroupOverview1.TabIndex = 48;
            // 
            // uscColumnOverview
            // 
            this.uscColumnOverview.Location = new System.Drawing.Point(384, 362);
            this.uscColumnOverview.Name = "uscColumnOverview";
            this.uscColumnOverview.Size = new System.Drawing.Size(198, 338);
            this.uscColumnOverview.TabIndex = 47;
            // 
            // uscDozenOverview
            // 
            this.uscDozenOverview.Location = new System.Drawing.Point(384, 43);
            this.uscDozenOverview.Name = "uscDozenOverview";
            this.uscDozenOverview.Size = new System.Drawing.Size(201, 313);
            this.uscDozenOverview.TabIndex = 46;
            // 
            // uscSequenceOverview
            // 
            this.uscSequenceOverview.Location = new System.Drawing.Point(12, 43);
            this.uscSequenceOverview.Name = "uscSequenceOverview";
            this.uscSequenceOverview.NumberSequence = ((System.Collections.Generic.IList<int>)(resources.GetObject("uscSequenceOverview.NumberSequence")));
            this.uscSequenceOverview.Size = new System.Drawing.Size(366, 657);
            this.uscSequenceOverview.TabIndex = 45;
            // 
            // uscLabel1
            // 
            this.uscLabel1.LabelText = "1-12";
            this.uscLabel1.Location = new System.Drawing.Point(334, 14);
            this.uscLabel1.Name = "uscLabel1";
            this.uscLabel1.Size = new System.Drawing.Size(81, 21);
            this.uscLabel1.TabIndex = 49;
            this.uscLabel1.Value = "";
            // 
            // uscLabel2
            // 
            this.uscLabel2.LabelText = "13-24";
            this.uscLabel2.Location = new System.Drawing.Point(411, 14);
            this.uscLabel2.Name = "uscLabel2";
            this.uscLabel2.Size = new System.Drawing.Size(81, 21);
            this.uscLabel2.TabIndex = 50;
            this.uscLabel2.Value = "";
            // 
            // btnLoadData
            // 
            this.btnLoadData.Location = new System.Drawing.Point(12, 11);
            this.btnLoadData.Name = "btnLoadData";
            this.btnLoadData.Size = new System.Drawing.Size(75, 23);
            this.btnLoadData.TabIndex = 52;
            this.btnLoadData.Text = "Hent...";
            this.btnLoadData.UseVisualStyleBackColor = true;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(12, 11);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 52;
            this.btnLoad.Text = "Hent...";
            this.btnLoad.UseVisualStyleBackColor = true;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(683, 735);
            this.Controls.Add(this.btnLoadData);
            this.Controls.Add(this.uscLabelPercentageHi);
            this.Controls.Add(this.uscLabelPercentageMe);
            this.Controls.Add(this.uscLabelPercentageLo);
            this.Controls.Add(this.uscGroupOverview1);
            this.Controls.Add(this.uscColumnOverview);
            this.Controls.Add(this.uscDozenOverview);
            this.Controls.Add(this.uscSequenceOverview);
            this.Controls.Add(this.btnGroups);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblNumbersDrawn2);
            this.Controls.Add(this.lblNumbersDrawn);
            this.Controls.Add(this.lblSequenceDesc);
            this.Controls.Add(this.btnReset);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(699, 740);
            this.Name = "FrmMain";
            this.Text = "Roulette Tracker v1.0";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label lblSequenceDesc;
        private System.Windows.Forms.Label lblNumbersDrawn;
        private System.Windows.Forms.Label lblNumbersDrawn2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnGroups;
        private UscSequenceOverview uscSequenceOverview;
        private UscDozenOverview uscDozenOverview;
        private UscColumnOverview uscColumnOverview;
        private UscGroupOverview uscGroupOverview1;
        private UscLabel uscLabelPercentageLo;
        private UscLabel uscLabel1;
        private UscLabel uscLabelPercentageMe;
        private UscLabel uscLabelPercentageHi;
        private UscLabel uscLabel2;
        private System.Windows.Forms.Button btnLoadData;
        private System.Windows.Forms.Button btnLoad;
    }
}

