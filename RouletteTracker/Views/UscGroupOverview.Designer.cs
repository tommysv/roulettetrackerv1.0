﻿using RouletteTracker.Controls;

namespace RouletteTracker.Views
{
    partial class UscGroupOverview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lwGroupOverview = new LwGroupOverview();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lwGroupOverview
            // 
            this.lwGroupOverview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lwGroupOverview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lwGroupOverview.FullRowSelect = true;
            this.lwGroupOverview.GridLines = true;
            this.lwGroupOverview.Location = new System.Drawing.Point(0, 0);
            this.lwGroupOverview.Name = "lwGroupOverview";
            this.lwGroupOverview.Size = new System.Drawing.Size(92, 675);
            this.lwGroupOverview.TabIndex = 44;
            this.lwGroupOverview.UseCompatibleStateImageBehavior = false;
            this.lwGroupOverview.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Gruppe";
            this.columnHeader1.Width = 85;
            // 
            // UscGroupOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lwGroupOverview);
            this.DoubleBuffered = true;
            this.Name = "UscGroupOverview";
            this.Size = new System.Drawing.Size(92, 675);
            this.ResumeLayout(false);

        }

        #endregion

        private LwGroupOverview lwGroupOverview;
        private System.Windows.Forms.ColumnHeader columnHeader1;

    }
}
