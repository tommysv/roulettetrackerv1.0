﻿namespace RouletteTracker.Views
{
    partial class DlgLoadData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoad = new System.Windows.Forms.Button();
            this.lbDataDescriptors = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnLoad
            // 
            this.btnLoad.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnLoad.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnLoad.Location = new System.Drawing.Point(0, 239);
            this.btnLoad.Name = "LoadButton";
            this.btnLoad.Size = new System.Drawing.Size(204, 23);
            this.btnLoad.TabIndex = 0;
            this.btnLoad.Text = "Hent";
            this.btnLoad.UseVisualStyleBackColor = true;
            // 
            // lbDataDescriptors
            // 
            this.lbDataDescriptors.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbDataDescriptors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDataDescriptors.FormattingEnabled = true;
            this.lbDataDescriptors.Location = new System.Drawing.Point(0, 0);
            this.lbDataDescriptors.Name = "lbDataDescriptors";
            this.lbDataDescriptors.Size = new System.Drawing.Size(204, 239);
            this.lbDataDescriptors.TabIndex = 1;
            this.lbDataDescriptors.SelectedValueChanged += new System.EventHandler(this.lbDataDescriptors_SelectedValueChanged);
            // 
            // DlgLoadData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(204, 262);
            this.Controls.Add(this.lbDataDescriptors);
            this.Controls.Add(this.btnLoad);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DlgLoadData";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Load data";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbDataDescriptors;
        private System.Windows.Forms.Button btnLoad;
    }
}