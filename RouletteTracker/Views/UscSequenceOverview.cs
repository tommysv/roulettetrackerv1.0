﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using RouletteTracker.Core;
using RouletteTracker.Core.Views;

namespace RouletteTracker.Views
{
    public partial class UscSequenceOverview : UserControl, IV_SequenceOverview
    {
        private IList<int> _numberSequence;
        private int[] _numberCount;

        public event AddSpinEventHandler AddSpin;

        public UscSequenceOverview()
        {
            InitializeComponent();

            _numberSequence = new List<int>();
            _numberCount =  new int[37];
        }

        public IList<int> NumberSequence
        {
            set
            {
                ClearView();
                _numberSequence = value;
                DrawNumberSequence();
            }
        }

        public void AddNumber(int number)
        {
            _numberSequence.Add(number);
            _numberCount[number] += 1;

            lwSequenceOverview.BeginUpdate();
            if (lwSequenceOverview.Items[number].SubItems.Count == 1)
            {
                lwSequenceOverview.Items[number].SubItems.Add(String.Format("{0}", _numberSequence.Count));
                lwSequenceOverview.Items[number].SubItems.Add(String.Format("{0}", _numberCount[number]));
            }
            else
            {
                lwSequenceOverview.Items[number].SubItems[1].Text += String.Format(" - {0}", _numberSequence.Count);
                lwSequenceOverview.Items[number].SubItems[2].Text = String.Format("{0}", _numberCount[number]);
            }
            lwSequenceOverview.EndUpdate();
        }

        public void DrawNumberSequence()
        {
            lwSequenceOverview.BeginUpdate();
            for (int n = 0; n < _numberSequence.Count; n++)
            {
                int number = _numberSequence[n];
                _numberCount[number] += 1;
                if (lwSequenceOverview.Items[number].SubItems.Count == 1)
                {
                    lwSequenceOverview.Items[number].SubItems.Add(String.Format("{0}", n + 1));
                    lwSequenceOverview.Items[number].SubItems.Add(String.Format("{0}", _numberCount[number]));
                }
                else
                {
                    lwSequenceOverview.Items[number].SubItems[1].Text += String.Format(" - {0}", n + 1);
                    lwSequenceOverview.Items[number].SubItems[2].Text = String.Format("{0}", _numberCount[number]);
                }
            }
            lwSequenceOverview.EndUpdate();
        }

        private void ClearView()
        {
            lwSequenceOverview.BeginUpdate();
            for (int nItem = 0; nItem < lwSequenceOverview.Items.Count; nItem++)
            {
                while (lwSequenceOverview.Items[nItem].SubItems.Count > 1)
                {
                    int lastIndex = lwSequenceOverview.Items[nItem].SubItems.Count - 1;
                    lwSequenceOverview.Items[nItem].SubItems.RemoveAt(lastIndex);
                }
            }
            lwSequenceOverview.EndUpdate();

            _numberSequence = new List<int>();
            _numberCount = new int[37];
        }

        public void ResetView()
        {
            ClearView();
        }

        protected void OnAddSpin(AddSpinEventArgs e)
        {
            if (AddSpin != null)
            {
                AddSpin(this, e);
            }
        }

        private void lwSequenceOverview_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewItem item = lwSequenceOverview.GetItemAt(e.X, e.Y);
            if (item != null)
            {
                OnAddSpin(new AddSpinEventArgs(item.Index));
            }
        }
    }
}
