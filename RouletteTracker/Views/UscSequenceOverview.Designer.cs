﻿using RouletteTracker.Controls;

namespace RouletteTracker.Views
{
    partial class UscSequenceOverview
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "0"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.MediumAquamarine, null);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "1"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "2"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {
            "3"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "4"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {
            "5"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem(new string[] {
            "6"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem(new string[] {
            "7"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem(new string[] {
            "8"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem(new string[] {
            "9"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem(new string[] {
            "10"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem(new string[] {
            "11"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem(new string[] {
            "12"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem(new string[] {
            "13"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem(new string[] {
            "14"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem(new string[] {
            "15"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem17 = new System.Windows.Forms.ListViewItem(new string[] {
            "16"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem18 = new System.Windows.Forms.ListViewItem(new string[] {
            "17"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem19 = new System.Windows.Forms.ListViewItem(new string[] {
            "18"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem20 = new System.Windows.Forms.ListViewItem(new string[] {
            "19"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem21 = new System.Windows.Forms.ListViewItem(new string[] {
            "20"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem22 = new System.Windows.Forms.ListViewItem(new string[] {
            "21"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem23 = new System.Windows.Forms.ListViewItem(new string[] {
            "22"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem24 = new System.Windows.Forms.ListViewItem(new string[] {
            "23"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem25 = new System.Windows.Forms.ListViewItem(new string[] {
            "24"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem26 = new System.Windows.Forms.ListViewItem(new string[] {
            "25"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem27 = new System.Windows.Forms.ListViewItem(new string[] {
            "26"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem28 = new System.Windows.Forms.ListViewItem(new string[] {
            "27"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem29 = new System.Windows.Forms.ListViewItem(new string[] {
            "28"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem30 = new System.Windows.Forms.ListViewItem(new string[] {
            "29"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem31 = new System.Windows.Forms.ListViewItem(new string[] {
            "30"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem32 = new System.Windows.Forms.ListViewItem(new string[] {
            "31"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem33 = new System.Windows.Forms.ListViewItem(new string[] {
            "32"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem34 = new System.Windows.Forms.ListViewItem(new string[] {
            "33"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem35 = new System.Windows.Forms.ListViewItem(new string[] {
            "34"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            System.Windows.Forms.ListViewItem listViewItem36 = new System.Windows.Forms.ListViewItem(new string[] {
            "35"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Black, null);
            System.Windows.Forms.ListViewItem listViewItem37 = new System.Windows.Forms.ListViewItem(new string[] {
            "36"}, -1, System.Drawing.SystemColors.Window, System.Drawing.Color.Red, null);
            this.lwSequenceOverview = new LwSequenceOverview();
            this.colNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colNumberPosition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDrawnPercent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lwSequenceOverview
            // 
            this.lwSequenceOverview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNumber,
            this.colNumberPosition,
            this.colDrawnPercent});
            this.lwSequenceOverview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lwSequenceOverview.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lwSequenceOverview.FullRowSelect = true;
            this.lwSequenceOverview.GridLines = true;
            this.lwSequenceOverview.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            listViewItem1.UseItemStyleForSubItems = false;
            listViewItem2.UseItemStyleForSubItems = false;
            listViewItem3.UseItemStyleForSubItems = false;
            listViewItem4.UseItemStyleForSubItems = false;
            listViewItem5.UseItemStyleForSubItems = false;
            listViewItem6.UseItemStyleForSubItems = false;
            listViewItem7.UseItemStyleForSubItems = false;
            listViewItem8.UseItemStyleForSubItems = false;
            listViewItem9.UseItemStyleForSubItems = false;
            listViewItem10.UseItemStyleForSubItems = false;
            listViewItem11.UseItemStyleForSubItems = false;
            listViewItem12.UseItemStyleForSubItems = false;
            listViewItem13.UseItemStyleForSubItems = false;
            listViewItem14.UseItemStyleForSubItems = false;
            listViewItem15.UseItemStyleForSubItems = false;
            listViewItem16.UseItemStyleForSubItems = false;
            listViewItem17.UseItemStyleForSubItems = false;
            listViewItem18.UseItemStyleForSubItems = false;
            listViewItem19.UseItemStyleForSubItems = false;
            listViewItem20.UseItemStyleForSubItems = false;
            listViewItem21.UseItemStyleForSubItems = false;
            listViewItem22.UseItemStyleForSubItems = false;
            listViewItem23.UseItemStyleForSubItems = false;
            listViewItem24.UseItemStyleForSubItems = false;
            listViewItem25.UseItemStyleForSubItems = false;
            listViewItem26.UseItemStyleForSubItems = false;
            listViewItem27.UseItemStyleForSubItems = false;
            listViewItem28.UseItemStyleForSubItems = false;
            listViewItem29.UseItemStyleForSubItems = false;
            listViewItem30.UseItemStyleForSubItems = false;
            listViewItem31.UseItemStyleForSubItems = false;
            listViewItem32.UseItemStyleForSubItems = false;
            listViewItem33.UseItemStyleForSubItems = false;
            listViewItem34.UseItemStyleForSubItems = false;
            listViewItem35.UseItemStyleForSubItems = false;
            listViewItem36.UseItemStyleForSubItems = false;
            listViewItem37.UseItemStyleForSubItems = false;
            this.lwSequenceOverview.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15,
            listViewItem16,
            listViewItem17,
            listViewItem18,
            listViewItem19,
            listViewItem20,
            listViewItem21,
            listViewItem22,
            listViewItem23,
            listViewItem24,
            listViewItem25,
            listViewItem26,
            listViewItem27,
            listViewItem28,
            listViewItem29,
            listViewItem30,
            listViewItem31,
            listViewItem32,
            listViewItem33,
            listViewItem34,
            listViewItem35,
            listViewItem36,
            listViewItem37});
            this.lwSequenceOverview.Location = new System.Drawing.Point(0, 0);
            this.lwSequenceOverview.Name = "lwSequenceOverview";
            this.lwSequenceOverview.Size = new System.Drawing.Size(366, 657);
            this.lwSequenceOverview.TabIndex = 16;
            this.lwSequenceOverview.UseCompatibleStateImageBehavior = false;
            this.lwSequenceOverview.View = System.Windows.Forms.View.Details;
            this.lwSequenceOverview.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lwSequenceOverview_MouseDoubleClick);
            // 
            // colNumber
            // 
            this.colNumber.Text = "Nummer";
            this.colNumber.Width = 84;
            // 
            // colNumberPosition
            // 
            this.colNumberPosition.Text = "Nummer - posisjon";
            this.colNumberPosition.Width = 225;
            // 
            // colDrawnPercent
            // 
            this.colDrawnPercent.Text = "Ant";
            this.colDrawnPercent.Width = 50;
            // 
            // UscSequenceOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lwSequenceOverview);
            this.DoubleBuffered = true;
            this.Name = "UscSequenceOverview";
            this.Size = new System.Drawing.Size(366, 657);
            this.ResumeLayout(false);

        }

        #endregion

        private LwSequenceOverview lwSequenceOverview;
        private System.Windows.Forms.ColumnHeader colNumber;
        private System.Windows.Forms.ColumnHeader colNumberPosition;
        private System.Windows.Forms.ColumnHeader colDrawnPercent;

    }
}
