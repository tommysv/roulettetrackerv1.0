﻿using System;
using System.Windows.Forms;
using RouletteTracker.Core.Views;

namespace RouletteTracker.Views
{
    public partial class FrmMain : Form, IV_Main
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        #region Properties
        public string NumSpins
        {
            set
            {
                lblNumbersDrawn2.Text = value;
            }
        }

        public string PercentageLo
        {
            set
            {
                uscLabelPercentageLo.Value = value;
            }
        }

        public string PercentageMe
        {
            set
            {
                uscLabelPercentageMe.Value = value;
            }
        }

        public string PercentageHi
        {
            set
            {
                uscLabelPercentageHi.Value = value;
            }
        }

        public IV_SequenceOverview SequenceOverview
        {
            get
            {
                return uscSequenceOverview;
            }
        }

        public IV_DozenOverview DozenOverview
        {
            get
            {
                return uscDozenOverview;
            }
        }

        public IV_ColumnOverview ColumnOverview
        {
            get
            {
                return uscColumnOverview;
            }
        }

        public IV_GroupOverview GroupOverview
        {
            get
            {
                return uscGroupOverview1;
            }
        }
        #endregion

        #region Events
        public event EventHandler Reset
        {
            add { btnReset.Click += value; }
            remove { btnReset.Click -= value; }
        }

        public event EventHandler LoadData
        {
            add { btnLoadData.Click += value; }
            remove { btnLoadData.Click -= value; }
        }

        public event EventHandler SaveData
        {
            add { btnSave.Click += value; }
            remove { btnSave.Click -= value; }
        }

        public event EventHandler Groups
        {
            add { btnGroups.Click += value; }
            remove { btnGroups.Click -= value; }
        }

        event EventHandler IV_Main.Load
        {
            add { this.Load += value; }
            remove { this.Load -= value; }
        }

        event FormClosedEventHandler IV_Main.FormClosed
        {
            add { this.FormClosed += value; }
            remove { this.FormClosed -= value; }
        }

        #endregion
    }
}
