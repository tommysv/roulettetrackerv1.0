﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using RouletteTracker.Core.Objects;
using RouletteTracker.Core.Views;

namespace RouletteTracker.Views
{
    public partial class UscGroupOverview : UserControl, IV_GroupOverview
    {
        private NumberGroups _numberGroups;

        public UscGroupOverview()
        {
            InitializeComponent();
        }

        public IList<int> NumberSequence
        {
            set
            {
                ResetView();
                DrawNumberSequence(value);
            }
        }

        public NumberGroups NumberGroups
        {
            set
            {
                _numberGroups = value;
            }
        }

        public void AddNumber(int number)
        {
            lwGroupOverview.BeginUpdate();
            DrawNumber(number);
            lwGroupOverview.EndUpdate();
        }

        private void DrawNumberSequence(IList<int> numberSequence)
        {
            lwGroupOverview.BeginUpdate();
            for (int n = 0; n < numberSequence.Count; n++)
            {
                DrawNumber(numberSequence[n]);
            }
            lwGroupOverview.EndUpdate();
        }

        private void DrawNumber(int number)
        {
            ListViewItem lwItem;
            switch (_numberGroups.FindNumberGroup(number))
            {
                case NumberGroup.Red:
                    lwItem = CreateListViewItem(String.Format("Rød - {0}", number), Color.Red, Color.White);
                    break;
                case NumberGroup.Green:
                    lwItem = CreateListViewItem(String.Format("Grønn - {0}", number), Color.Green, Color.White);
                    break;
                case NumberGroup.Black:
                    lwItem = CreateListViewItem(String.Format("Sort - {0}", number), Color.Black, Color.White);
                    break;
                default:
                    lwItem = CreateListViewItem(String.Format("Ingen - {0}", number), Color.White, Color.Black);
                    break;
            }
            if (lwGroupOverview.Items.Count == 0)
                lwGroupOverview.Items.Add(lwItem);
            else
                lwGroupOverview.Items.Insert(0, lwItem);
            lwGroupOverview.Items[0].EnsureVisible();
        }

        private ListViewItem CreateListViewItem(string text, Color backcolor, Color forecolor)
        {
            ListViewItem lwItem = new ListViewItem(text);
            lwItem.BackColor = backcolor;
            lwItem.ForeColor = forecolor;
            return lwItem;
        }

        public void ResetView()
        {
            lwGroupOverview.BeginUpdate();
            lwGroupOverview.Items.Clear();
            lwGroupOverview.EndUpdate();
        }
    }
}
